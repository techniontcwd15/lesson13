<?php 

function a () { 
  function b() { 
    echo "I am b.\n"; 
  } 
  echo "I am a.\n"; 
} 
if (function_exists("b")) echo "b is defined.\n"; else echo "b is not defined.\n"; 
a(); 
if (function_exists("b")) echo "b is defined.\n"; else echo "b is not defined.\n"; 

?> 
<?php 

function a() { 
    if ( ! function_exists('b') ) { 
        function b() { 
            echo 'I am b'; 
        } 
    } 
    echo 'I am a'; 
} 
?>
<?php
function recursion($a)
{
    if ($a < 20) {
        echo "$a\n";
        recursion($a + 1);
    }
}
?>
