<?php 

$host = "localhost";
$dbname = "somedb";
$username = "root";
$password = "1";


// PDO
$pdo = new PDO("mysql:host=".$host.";dbname=".$dbname, $username, $password);
 
// mysqli, procedural way
$conn = mysqli_connect($host,$username,$password,$dbname);
 
// mysqli, object oriented way
$mysqli = new mysqli($host,$username,$password,$dbname);

////Simple Example/////
$sqlQuery = 'SELECT name FROM users';
$sqlInsert = "INSERT INTO table (a,b,c) VALUES (1,2,3)";


// mysqli procedural
if (mysqli_query($conn, $sqlInsert)) {
    echo "New record created successfully";
} else {
    echo "Error: " . $sqlInsert . "<br>" . mysqli_error($conn);
}

$result = mysqli_query($conn, $sql);

if (mysqli_num_rows($result) > 0) {
    // output data of each row
    while($row = mysqli_fetch_assoc($result)) {
        echo "id: " . $row["id"]. " - Name: " . $row["firstname"]. " " . $row["lastname"]. "<br>";
    }
} else {
    echo "0 results";
}

// mysqli object oriented
if ($mysqli->query($sqlInsert)) {
    echo "New record created successfully";
} else {
    echo "Error: " . $sqlInsert . "<br>" . $conn->error;
}

$result = $mysqli->query($sqlQuery);

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        echo "id: " . $row["id"]. " - Name: " . $row["firstname"]. " " . $row["lastname"]. "<br>";
    }
} else {
    echo "0 results";
}



// pdo
$stmt = $pdo->query($sqlQuery);
while ($row = $stmt->fetch())
{
  echo $row['name'] . "\n";
}

$pdo->exec($sql);

// prepare

// mysqli
$query = $mysqli->prepare('
    SELECT * FROM users
    WHERE username = ?
    AND email = ?
    AND last_login > ?');
     
$query->bind_param('sss', 'test', $mail, time() - 3600);
$query->execute();

// pdo
$params = array(':username' => 'test', ':email' => $mail, ':last_login' => time() - 3600);
     
$pdo->prepare('
    SELECT * FROM users
    WHERE username = :username
    AND email = :email
    AND last_login > :last_login');
     
$pdo->execute($params);

// Object Mapping
class User {
    public $id;
    public $first_name;
    public $last_name;
     
    public function info()
    {
        return '#'.$this->id.': '.$this->first_name.' '.$this->last_name;
    }
}

$query = "SELECT id, first_name, last_name FROM users";
     
// PDO
$result = $pdo->query($query);
$result->setFetchMode(PDO::FETCH_CLASS, 'User');
 
while ($user = $result->fetch()) {
    echo $user->info()."\n";
}
// MySQLI, procedural way
if ($result = mysqli_query($mysqli, $query)) {
    while ($user = mysqli_fetch_object($result, 'User')) {
        echo $user->info()."\n";
    }
}
// MySQLi, object oriented way
if ($result = $mysqli->query($query)) {
    while ($user = $result->fetch_object('User')) {
        echo $user->info()."\n";
    }
}

// security 
$username = PDO::quote($_GET['username']);
$username = mysqli_real_escape_string($_GET['username']);



?>