<?php session_start(); ?>
<html>
    <head>
        <title>X - O Game</title>
    </head>
    <body>
        <?php if ($_GET): ?>
            <?php
            $_SESSION['board'][$_GET['x']][$_GET['y']] = $_GET['s'];
            if ($_GET['s'] == "x") {
                $ops_s = "o";
            } else {
                $ops_s = "x";
            }
            ?>
            <table>
                <tr>
                    <td>
                        <?php if (isset($_SESSION['board'][1][1])): ?>
                            <?php echo $_SESSION['board'][1][1]; ?>
                        <?php else: ?>
                            <a href="?s=<?= $ops_s; ?>&x=1&y=1">?</a>
                        <?php endif; ?>
                    </td>
                    <td>
                        <?php if (isset($_SESSION['board'][2][1])): ?>
                            <?php echo $_SESSION['board'][2][1]; ?>
                        <?php else: ?>
                            <a href="?s=<?= $ops_s; ?>&x=2&y=1">?</a>
                        <?php endif; ?>
                    </td>
                    <td>
                        <?php if (isset($_SESSION['board'][3][1])): ?>
                            <?php echo $_SESSION['board'][3][1]; ?>
                        <?php else: ?>
                            <a href="?s=<?= $ops_s; ?>&x=3&y=1">?</a>
                        <?php endif; ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?php if (isset($_SESSION['board'][1][2])): ?>
                            <?php echo $_SESSION['board'][1][2]; ?>
                        <?php else: ?>
                            <a href="?s=<?= $ops_s; ?>&x=1&y=2">?</a>
                        <?php endif; ?>
                    </td>
                    <td>
                        <?php if (isset($_SESSION['board'][2][2])): ?>
                            <?php echo $_SESSION['board'][2][2]; ?>
                        <?php else: ?>
                            <a href="?s=<?= $ops_s; ?>&x=2&y=2">?</a>
                        <?php endif; ?>
                    </td>
                    <td>
                        <?php if (isset($_SESSION['board'][3][2])): ?>
                            <?php echo $_SESSION['board'][3][2]; ?>
                        <?php else: ?>
                            <a href="?s=<?= $ops_s; ?>&x=3&y=2">?</a>
                        <?php endif; ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?php if (isset($_SESSION['board'][1][3])): ?>
                            <?php echo $_SESSION['board'][1][3]; ?>
                        <?php else: ?>
                            <a href="?s=<?= $ops_s; ?>&x=1&y=3">?</a>
                        <?php endif; ?>
                    </td>
                    <td>
                        <?php if (isset($_SESSION['board'][2][3])): ?>
                            <?php echo $_SESSION['board'][2][3]; ?>
                        <?php else: ?>
                            <a href="?s=<?= $ops_s; ?>&x=2&y=3">?</a>
                        <?php endif; ?>
                    </td>
                    <td>
                        <?php if (isset($_SESSION['board'][3][3])): ?>
                            <?php echo $_SESSION['board'][3][3]; ?>
                        <?php else: ?>
                            <a href="?s=<?= $ops_s; ?>&x=3&y=3">?</a>
                        <?php endif; ?>
                    </td>
                </tr>
            </table>
            <?php
            if (isset($_SESSION['board'][1][1]) && isset($_SESSION['board'][1][2]) && isset($_SESSION['board'][1][3]) && (($_SESSION['board'][1][1] == $_SESSION['board'][1][2]) && ($_SESSION['board'][1][2] == $_SESSION['board'][1][3]))):
                echo "<p>Game Over " . $_SESSION['board'][1][1] . " Win!!</p>";
            endif;
            if ((isset($_SESSION['board'][1][1]) && isset($_SESSION['board'][2][2]) && isset($_SESSION['board'][3][3]) && ($_SESSION['board'][1][1] == $_SESSION['board'][2][2]) && ($_SESSION['board'][2][2] == $_SESSION['board'][3][3]))):
                echo "<p>Game Over " . $_SESSION['board'][1][1] . " Win!!</p>";
            endif;
            if ((isset($_SESSION['board'][2][1]) && isset($_SESSION['board'][2][2]) && isset($_SESSION['board'][2][3]) && ($_SESSION['board'][2][1] == $_SESSION['board'][2][2]) && ($_SESSION['board'][2][2] == $_SESSION['board'][2][3]))):
                echo "<p>Game Over " . $_SESSION['board'][2][1] . " Win!!</p>";
            endif;
            if ((isset($_SESSION['board'][3][1]) && isset($_SESSION['board'][3][2]) && isset($_SESSION['board'][3][3]) && ($_SESSION['board'][3][1] == $_SESSION['board'][3][2]) && ($_SESSION['board'][3][2] == $_SESSION['board'][3][3]))):
                echo "<p>Game Over " . $_SESSION['board'][3][1] . " Win!!</p>";
            endif;
            if ((isset($_SESSION['board'][1][3]) && isset($_SESSION['board'][2][3]) && isset($_SESSION['board'][3][3]) && ($_SESSION['board'][1][3] == $_SESSION['board'][2][3]) && ($_SESSION['board'][2][3] == $_SESSION['board'][3][3]))):
                echo "<p>Game Over " . $_SESSION['board'][1][3] . " Win!!</p>";
            endif;
            if ((isset($_SESSION['board'][1][2]) && isset($_SESSION['board'][2][2]) && isset($_SESSION['board'][3][3]) && ($_SESSION['board'][1][2] == $_SESSION['board'][2][2]) && ($_SESSION['board'][2][2] == $_SESSION['board'][3][2]))):
                echo "<p>Game Over " . $_SESSION['board'][1][2] . " Win!!</p>";
            endif;
            if ((isset($_SESSION['board'][1][1]) && isset($_SESSION['board'][2][1]) && isset($_SESSION['board'][3][1]) && ($_SESSION['board'][1][1] == $_SESSION['board'][2][1]) && ($_SESSION['board'][2][1] == $_SESSION['board'][3][1]))):
                echo "<p>Game Over " . $_SESSION['board'][1][1] . " Win!!</p>";
            endif;
            ?>
            <?php
        else:
            $_SESSION['board'] = array();
            ?>
            <table>
                <tr><td><a href="?s=x&x=1&y=1">?</a></td>
                    <td><a href="?s=x&x=2&y=1">?</a></td>
                    <td><a href="?s=x&x=3&y=1">?</a></td>
                </tr>
                <tr><td><a href="?s=x&x=1&y=2">?</a></td>
                    <td><a href="?s=x&x=2&y=2">?</a></td>
                    <td><a href="?s=x&x=3&y=2">?</a></td>
                </tr>
                <tr><td><a href="?s=x&x=1&y=3">?</a></td>
                    <td><a href="?s=x&x=2&y=3">?</a></td>
                    <td><a href="?s=x&x=3&y=3">?</a></td>
                </tr>
            </table>
        <?php endif; ?>
    </body>
</html>