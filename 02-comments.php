<?php
echo 'This is a test'; // This is a one-line c++ style comment
/* This is a multi line comment
  yet another line of comment */
echo 'This is yet another test';
echo 'One Final Test'; # This is a one-line shell-style comment
?>

<h1>This is an <?php # echo 'simple';    ?> example</h1>
<p>The header above will say 'This is an  example'.</p>

<?php
/*
  echo 'This is a test'; /* This comment will cause a problem */
*/
?>

<?php
//*
echo "first";
//*/
echo "secund";
?>
<?php
/*
  echo "first";
  // */
echo "secund";
?>
<?php
//*
echo "first";
/* /
  echo "secund";
  // */
?>
<?php
/*
  echo "first";
  / */
echo "secund";
//*/
?>
<!-- comment
<?php echo 'test2' ?>
-->
