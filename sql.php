<?php

$mysqli = new mysqli("localhost", "amit_class1", "class1", "amit_class1");

/* check connection */
if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}

/* change character set to utf8 */
if (!$mysqli->set_charset("utf8")) {
    printf("Error loading character set utf8: %s\n", $mysqli->error);
} else {
    printf("Current character set: %s\n", $mysqli->character_set_name());
}

$mysqli->close();


$sql = "CREATE TABLE Persons 
(
PID INT NOT NULL AUTO_INCREMENT, 
PRIMARY KEY(PID),
FirstName CHAR(15),
LastName CHAR(15),
Age INT
)";

// CREATE TABLE IF NOT EXISTS Persons 
// (
// PID INT NOT NULL AUTO_INCREMENT, 
// PRIMARY KEY(PID),
// FirstName CHAR(15),
// LastName CHAR(15),
// Age INT
// )

/* Select queries return a resultset */
if ($result = $mysqli->query("SELECT Name FROM City LIMIT 10")) {
    printf("Select returned %d rows.\n", $result->num_rows);

    /* free result set */
    $result->close();
}

$mysqli->close();

mysqli_query($con,"INSERT INTO Persons (FirstName, LastName, Age)
VALUES ('Peter', 'Griffin',35)");

mysqli_query($con,"DELETE FROM Persons WHERE LastName='Griffin'");

$sql = "SELECT * FROM tbl_usr"; 

    if ($result = $mysqli->query($sql)) { 
        while($obj = $result->fetch_object()){ 
            $line.=$obj->uid; 
            $line.=$obj->role; 
            $line.=$obj->roleid; 
        } 
    } 
    $result->close(); 
    unset($obj); 
    unset($sql); 
    unset($query); 

?>
